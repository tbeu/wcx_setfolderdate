SetFolderDate plugin 1.3.0.0 for Total Commander
================================================

 * License:
-----------

This software is released as freeware.


 * Disclaimer:
--------------

This software is provided "AS IS" without any warranty, either expressed or
implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not be
liable for any special, incidental, consequential or indirect damages due to
loss of data or any other reason.


 * Installation:
----------------

Open the plugin archive using Total Commander and installation will start.


 * Description:
---------------

SetFolderDate is a utility packer plugin for Total Commander to recursively set
the timestamp of directory structures from the last modified/write time of the
youngest file inside. Alternatively, the timestamp of the oldest file can be
taken.


 * ChangeLog:
-------------

 o Version 1.3.0.0 (02.05.2020)
   - added configuration option to skip all directories
   - added alternative extension "dirdate" besides "setfolderdate"
   - minor code improvements
 o Version 1.2.0.0 (24.08.2019)
   - added support of background operation
   - improved reliability of retrieving last write time
 o Version 1.1.0.0 (19.07.2019)
   - added configuration option to skip empty directories
   - added configuration option to use oldest file alternatively
   - improved reliability of retrieving last write time
 o Version 1.0.0.5 (12.07.2019)
   - again fixed support of long directory/file names
   - added progress bar
   - improved tree creation performance
 o Version 1.0.0.4 (08.07.2019)
   - fixed support of long directory/file names
 o Version 1.0.0.3 (07.07.2019)
   - code refactoring
 o Version 1.0.0.2 (06.07.2019)
   - minor code improvements
 o Version 1.0.0.1 (03.07.2019)
   - first public release


 * References:
--------------

 o WCX Writer's Reference by Christian Ghisler & Jiri Barton
   - http://ghisler.fileburst.com/plugins/wcx_ref2.21se.zip


 * Trademark and Copyright Statements:
--------------------------------------

 o Total Commander is Copyright © 1993-2020 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net