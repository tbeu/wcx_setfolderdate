#pragma once

#include <string>

struct Config {
	Config() = delete;
	Config(const wchar_t* _iniFileName) {
		iniFileName = _iniFileName;
		ignoreAllDirs = GetPrivateProfileIntW(iniSectionName, iniIgnoreAllDirs, 0, _iniFileName);
		ignoreEmptyDirs = GetPrivateProfileIntW(iniSectionName, iniIgnoreEmptyDirs, 0, _iniFileName);
		setMin = GetPrivateProfileIntW(iniSectionName, iniSetMin, 0, _iniFileName);
	}
	inline void Write() {
		WritePrivateProfileStringW(iniSectionName, iniIgnoreAllDirs, ignoreAllDirs ? L"1" : L"0", iniFileName.c_str());
		WritePrivateProfileStringW(iniSectionName, iniIgnoreEmptyDirs, ignoreEmptyDirs ? L"1" : L"0", iniFileName.c_str());
		WritePrivateProfileStringW(iniSectionName, iniSetMin, setMin ? L"1" : L"0", iniFileName.c_str());
	}
	bool setMin{ false };
	bool ignoreEmptyDirs{ false };
	bool ignoreAllDirs{ false };
private:
	std::wstring iniFileName{};
	static constexpr wchar_t iniSectionName[23]{ L"SetFolderDate Settings" };
	static constexpr wchar_t iniIgnoreAllDirs[21]{ L"IgnoreAllDirectories" };
	static constexpr wchar_t iniIgnoreEmptyDirs[23]{ L"IgnoreEmptyDirectories" };
	static constexpr wchar_t iniSetMin[14]{ L"UseOldestFile" };
};
