// see https://stackoverflow.com/a/236803/8520615

#pragma once

#include <sstream>
#include <string>
#include <vector>

template<typename T>
using StringType = std::basic_string<T, std::char_traits<T>, std::allocator<T>>;

template<typename T>
using StringStreamType = std::basic_stringstream<T, std::char_traits<T>, std::allocator<T>>;

template<typename T, typename Out>
inline static void SplitT(const StringType<T>& s, T delim, Out result) {
	StringStreamType<T> ss(s);
	StringType<T> item;
	while (std::getline(ss, item, delim)) {
		*(result++) = std::move(item);
	}
}

template <typename T>
inline static void SplitT(const StringType<T>& s, std::vector<StringType<T>>& elems, T delim) {
	SplitT<T, std::back_insert_iterator<std::vector<StringType<T>>>>(s, delim, std::back_inserter(elems));
}

constexpr auto Split = SplitT<char>;
constexpr auto SplitW = SplitT<wchar_t>;
