// simple tree implementation

#pragma once

#include "allocate_unique.h"
#include <algorithm>
#include <execution>
#include <functional>
#include <memory>
#include <string>
#include <vector>

struct Node;
using NodeAlloc = std::allocator<Node>;
using D = alloc_deleter<NodeAlloc>;
using NodeRef = std::unique_ptr<Node, D>;
using NodeRefAlloc = std::allocator<NodeRef>;
using NodeIt = std::vector<NodeRef, NodeRefAlloc>::iterator;
using NodeCit = std::vector<NodeRef, NodeRefAlloc>::const_iterator;
using NodeRit = std::vector<NodeRef, NodeRefAlloc>::reverse_iterator;
using CmpNodePredFunc = std::function<bool (const NodeRef&, const NodeRef&)>;
constexpr auto MaxNodeFunc = std::max_element<NodeCit, CmpNodePredFunc>;
constexpr auto MinNodeFunc = std::min_element<NodeCit, CmpNodePredFunc>;

struct Node {
	Node() {}
	Node(const std::wstring& _name) : name(_name) {}
	Node(std::wstring&& _name) noexcept : name(std::move(_name)) {}

	inline bool empty() const noexcept {
		return nodes.empty();
	}

	inline NodeIt AddNode(const NodeAlloc& alloc, NodeCit it, const std::wstring& _name) {
		return nodes.emplace(it, allocate_unique<Node>(alloc, _name));
	}
	inline NodeIt AddNode(const NodeAlloc& alloc, NodeCit it, std::wstring&& _name) {
		return nodes.emplace(it, allocate_unique<Node>(alloc, std::move(_name)));
	}

	inline NodeIt FindNode(const std::wstring& _name) noexcept {
		return std::find_if(nodes.begin(), nodes.end(), [&_name](const NodeRef& node) {
			return node->name == _name;
		});
	}
	inline NodeRit rFindNode(const std::wstring& _name) noexcept {
		return std::find_if(nodes.rbegin(), nodes.rend(), [&_name](const NodeRef& node) {
			return node->name == _name;
		});
	}
	inline NodeCit LowerBound(const NodeAlloc& alloc, const std::wstring& _name) noexcept {
		auto node = allocate_unique<Node>(alloc, _name);
		return std::lower_bound(nodes.cbegin(), nodes.cend(), node, [](const NodeRef& node1, const NodeRef& node2) {
			return node1->name < node2->name;
		});
	}

	const std::wstring name{};
	std::vector<NodeRef, NodeRefAlloc> nodes{};
	bool isDir{ false };
};
