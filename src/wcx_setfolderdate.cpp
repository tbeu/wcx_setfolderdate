// wcx_setfolderdate.cpp : Definiert die exportierten Funktionen für die DLL-Anwendung.
//

#include "stdafx.h"
//#define PERFORMANCE_MEASURE 1
//#define CONGIG_MUTEX 1
#include "config.h"
#include "configdlg.h"
#include "cunicode.h"
#include "node.h"
#include "splitstring.h"
#include "resource.h"
#include "wcxhead.h" // WCX
#include <commctrl.h>
#include <algorithm>
#if defined(PERFORMANCE_MEASURE)
#include <chrono>
#endif
#include <filesystem>
#if defined (CONGIG_MUTEX)
#include <shared_mutex>
#endif

#if defined(PERFORMANCE_MEASURE)
namespace chrono = std::chrono;
#endif
namespace fs = std::filesystem;

class RuntimeErrorAbort : std::runtime_error {
	using std::runtime_error::runtime_error;
};

struct RuntimeErrorPath : std::runtime_error {
	RuntimeErrorPath(const std::string& _msg, const fs::path& _path) : std::runtime_error(_msg), path(_path) {}
	fs::path path;
};

static std::wstring iniFileName{};
#if defined (CONGIG_MUTEX)
static std::shared_timed_mutex configMutex;
#endif
static tProcessDataProc pProcessDataProc{ nullptr };
//static tChangeVolProc pChangeVolProc{ nullptr };
static tProcessDataProcW pProcessDataProcW{ nullptr };
//static tChangeVolProcW pChangeVolProcW{ nullptr };

inline static fs::path operator/(const fs::path& _Left, const NodeRef& _Right) {
	return (fs::path(_Left) /= _Right->name);
}

static NodeCit AddNode(const NodeAlloc& alloc, const NodeRef& node, const std::vector<std::wstring>& elems, unsigned i) {
	auto& elem = elems[i++];
	auto it{ node->LowerBound(alloc, elem) };
	if (it == node->nodes.cend() || (*it)->name != elem) {
		it = node->AddNode(alloc, it, std::move(elem));
	}
	if (i < elems.size()) {
		it = AddNode(alloc, *it, elems, i);
	}
	return it;
}

inline static void TimetToFileTime(time_t t, LPFILETIME pft) {
	const auto ll{ Int32x32To64(t, 10000000) + 116444736000000000 };
	pft->dwLowDateTime = static_cast<DWORD>(ll);
	pft->dwHighDateTime = ll >> 32;
}

inline static void GetLastWriteTime(const fs::path& path, LPFILETIME pft) {
	{
		WIN32_FIND_DATAW fd;
		if (auto hFind{ FindFirstFileT(path.wstring().c_str(), &fd) }; INVALID_HANDLE_VALUE != hFind) {
			*pft = fd.ftLastWriteTime;
			::FindClose(hFind);
			return;
		}
	}
	// fallback
	if (auto hFile{ CreateFileT(path.wstring().c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, nullptr) }; INVALID_HANDLE_VALUE != hFile) {
		const auto rc{ ::GetFileTime(hFile, nullptr, nullptr, pft) };
		::CloseHandle(hFile);
		if (0 != rc) {
			return;
		}
	}
	// fallback
	if (wchar_t lpath[wdirtypemax + longnameprefixmax]; MakeExtraLongNameW(lpath, path.wstring().c_str(), wdirtypemax + longnameprefixmax)) {
		if (struct _stat64 fileInfo; 0 == _wstati64(lpath, &fileInfo)) {
			TimetToFileTime(fileInfo.st_mtime, pft);
			return;
		}
	}
	throw RuntimeErrorPath("Failed to get last write time.", path);
}

inline static void SetLastWriteTime(const fs::path& path, LPFILETIME pft) {
	if (auto hFile{ CreateFileT(path.wstring().c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, nullptr) }; INVALID_HANDLE_VALUE != hFile) {
		const auto rc{ ::SetFileTime(hFile, nullptr, nullptr, pft) };
		::CloseHandle(hFile);
		if (0 != rc) {
			return;
		}
	}
	throw RuntimeErrorPath("Failed to set last write time.", path);
}

inline static void SetFolderDate(const fs::path& path, const NodeRef& node) {
	FILETIME ft;
	GetLastWriteTime(path / node, &ft);
	SetLastWriteTime(path, &ft);
}

inline static bool CompareFileTime(const fs::path& path1, const fs::path& path2) {
	FILETIME ft1, ft2;
	GetLastWriteTime(path1, &ft1);
	GetLastWriteTime(path2, &ft2);
	return -1L == ::CompareFileTime(&ft1, &ft2);
}

template<NodeCit minmax(NodeCit, NodeCit, CmpNodePredFunc)>
static void SetFolderDate(const fs::path& path, std::vector<NodeRef, NodeRefAlloc>& nodes, const Config& config) {
	// depth-first
	for (const auto& node : nodes) {
		if (node->isDir) {
			SetFolderDate<minmax>(path / node, node->nodes, config);
		}
	}

	// can delete nodes, since not needed afterwards
	if (config.ignoreAllDirs) {
		nodes.erase(std::remove_if(nodes.begin(), nodes.end(), [](const NodeRef& node) {
			return node->isDir; }), nodes.end());
	}
	else if (config.ignoreEmptyDirs) {
		nodes.erase(std::remove_if(nodes.begin(), nodes.end(), [](const NodeRef& node) {
			return node->isDir && node->empty(); }), nodes.end());
	}

	if (const auto it{ minmax(nodes.cbegin(), nodes.cend(), [&path](const NodeRef& node1, const NodeRef& node2) {
		return CompareFileTime(path / node1, path / node2);
	}) }; it != nodes.cend()) {
		SetFolderDate(path, *it);
	}

	if (pProcessDataProcW && 0 == pProcessDataProcW(path.wstring().c_str(), 0)) {
		throw RuntimeErrorAbort("User abort");
	}
}

constexpr auto SetFolderDateMax = SetFolderDate<MaxNodeFunc>;
constexpr auto SetFolderDateMin = SetFolderDate<MinNodeFunc>;

inline static void SetFolderDate(const NodeRef& root, const Config& config) {
	if (config.setMin) {
		for (const auto& node : root->nodes) {
			SetFolderDateMin(fs::path(root->name) / node, node->nodes, config);
		}
	}
	else {
		for (const auto& node : root->nodes) {
			SetFolderDateMax(fs::path(root->name) / node, node->nodes, config);
		}
	}
}

// packer plugin function declarations
HANDLE __stdcall OpenArchive(tOpenArchiveData *ArchiveData);
HANDLE __stdcall OpenArchiveW(tOpenArchiveDataW *ArchiveData);
int __stdcall CloseArchive(HANDLE hArcData);
int __stdcall ReadHeader(HANDLE hArcData, tHeaderData *HeaderData);
int __stdcall ReadHeaderExW(HANDLE hArcData, tHeaderDataExW *HeaderData);
int __stdcall ProcessFile(HANDLE hArcData, int Operation, char *DestPath, char *DestName);
int __stdcall ProcessFileW(HANDLE hArcData, int Operation, wchar_t* DestPath, wchar_t* DestName);
int __stdcall GetPackerCaps(void);
void __stdcall SetChangeVolProc(HANDLE hArcData, tChangeVolProc pChangeVolProc1);
void __stdcall SetChangeVolProcW(HANDLE hArcData, tChangeVolProcW pChangeVolProc1);
void __stdcall SetProcessDataProc(HANDLE hArcData, tProcessDataProc pProcessDataProc1);
void __stdcall SetProcessDataProcW(HANDLE hArcData, tProcessDataProcW pProcessDataProc1);
void __stdcall ConfigurePacker(HWND Parent, HINSTANCE DllInstance);
void __stdcall PackSetDefaultParams(PackDefaultParamStruct* dps);
void __stdcall PackSetDefaultParamsW(PackDefaultParamStructW* dps);
int __stdcall DeleteFiles(char *PackedFile, char *DeleteList);
int __stdcall DeleteFilesW(wchar_t* PackedFile, wchar_t* DeleteList);
int __stdcall PackFiles(char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags);
int __stdcall PackFilesW(wchar_t* PackedFile, wchar_t* SubPath, wchar_t* SrcPath, wchar_t* AddList, int Flags);

HANDLE __stdcall OpenArchive(tOpenArchiveData *ArchiveData) {
	ArchiveData->OpenResult = E_UNKNOWN_FORMAT;
	return nullptr;
}

HANDLE __stdcall OpenArchiveW(tOpenArchiveDataW *ArchiveData) {
	ArchiveData->OpenResult = E_UNKNOWN_FORMAT;
	return nullptr;
}

int __stdcall CloseArchive(HANDLE hArcData) {
	return 0;
}

int __stdcall ReadHeader(HANDLE hArcData, tHeaderData *HeaderData) {
	return E_BAD_ARCHIVE;
}

int __stdcall ReadHeaderExW(HANDLE hArcData, tHeaderDataExW *HeaderData) {
	return E_BAD_ARCHIVE;
}

int __stdcall ProcessFile(HANDLE hArcData, int Operation, char *DestPath, char *DestName) {
	return E_BAD_ARCHIVE;
}

int __stdcall ProcessFileW(HANDLE hArcData, int Operation, wchar_t* DestPath, wchar_t* DestName) {
	return E_BAD_ARCHIVE;
}

int __stdcall DeleteFiles(char *PackedFile, char *DeleteList) {
	return E_NOT_SUPPORTED;
}

int __stdcall DeleteFilesW(wchar_t* PackedFile, wchar_t* DeleteList) {
	return E_NOT_SUPPORTED;
}

int __stdcall GetBackgroundFlags(void) {
	return BACKGROUND_PACK;
}

int __stdcall GetPackerCaps(void) {
	return PK_CAPS_NEW | PK_CAPS_MULTIPLE | PK_CAPS_MODIFY | PK_CAPS_HIDE | PK_CAPS_OPTIONS;
}

void __stdcall SetChangeVolProc(HANDLE hArcData, tChangeVolProc pChangeVolProc1) {
	//pChangeVolProc = pChangeVolProc1;
}

void __stdcall SetChangeVolProcW(HANDLE hArcData, tChangeVolProcW pChangeVolProc1) {
	//pChangeVolProcW = pChangeVolProc1;
}

void __stdcall SetProcessDataProc(HANDLE hArcData, tProcessDataProc pProcessDataProc1) {
	pProcessDataProc = pProcessDataProc1;
}

void __stdcall SetProcessDataProcW(HANDLE hArcData, tProcessDataProcW pProcessDataProc1) {
	pProcessDataProcW = pProcessDataProc1;
}

int __stdcall PackFiles(char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags) {
	wchar_t* AddListW{ nullptr };
	if (AddList) {
		size_t s{};
		do {
			s += std::strlen(AddList) + 1;
		} while (*AddList);
		std::vector<wchar_t> vAddListW{};
		vAddListW.reserve(++s);
		AddListW = vAddListW.data();
		ZeroMemory(AddListW, s*(sizeof(wchar_t)));
		do {
			const auto len{ std::strlen(AddList) + 1 };
			awlcopy(AddListW, AddList, static_cast<int>(len));
			AddListW += len;
			AddList += len;
		} while (*AddList);
		AddListW = vAddListW.data();
	}

	std::vector<wchar_t> PackedFileW{};
	std::vector<wchar_t> SubPathW{};
	std::vector<wchar_t> SrcPathW{};
	PackedFileW.reserve(std::strlen(PackedFile) + 1);
	SubPathW.reserve(std::strlen(SubPath) + 1);
	SrcPathW.reserve(std::strlen(SrcPath) + 1);
	return PackFilesW(awlcopy(PackedFileW.data(), PackedFile, static_cast<int>(PackedFileW.capacity())), awlcopy(SubPathW.data(), SubPath, static_cast<int>(SubPathW.capacity())), awlcopy(SrcPathW.data(), SrcPath, static_cast<int>(SrcPathW.capacity())), AddListW, Flags);
}

int __stdcall PackFilesW(wchar_t* PackedFile, wchar_t* SubPath, wchar_t* SrcPath, wchar_t* AddList, int Flags) {
	int rc{ 0 };

	if (nullptr != SubPath || nullptr == SrcPath) {
		return E_EWRITE;
	}

	if (AddList) {
		try {
#if defined (CONGIG_MUTEX)
			std::shared_lock<std::shared_timed_mutex> rLock(configMutex);
#endif
			Config config{ iniFileName.c_str() };
			NodeAlloc alloc{};
			auto root{ allocate_unique<Node>(alloc, SrcPath) };
			std::vector<std::wstring> pathElems{};
#if defined(PERFORMANCE_MEASURE)
			unsigned count{};
			auto startTime{ chrono::system_clock::now() };
#endif
			// process AddList to fill tree
			do {
				SplitW(AddList, pathElems, fs::path::preferred_separator);
				const auto node{ AddNode(alloc, root, pathElems, 0) };
				const auto len{ std::wcslen(AddList) };
				(*node)->isDir = fs::path::preferred_separator == AddList[len - 1];
				pathElems.clear();
				AddList += len + 1;
#if defined(PERFORMANCE_MEASURE)
				count++;
#endif
			} while (*AddList);
#if defined(PERFORMANCE_MEASURE)
			auto endTime{ chrono::system_clock::now() };
			auto duration{ chrono::duration_cast<chrono::milliseconds>(endTime - startTime).count() };
			wchar_t buf[128];
			swprintf(buf, 128, L"tree: %i\ncount: %u", (int)duration, count);
			MessageBoxT(nullptr, buf, L"tree", MB_OK | MB_ICONERROR);
#endif
			SetFolderDate(root, config);
		}
		catch (RuntimeErrorAbort) {
			rc = E_EABORTED;
		}
		catch (const RuntimeErrorPath& e) {
			const std::string& msg { e.what() };
			std::wstring wmsg;
			std::copy(msg.cbegin(), msg.cend(), std::back_inserter(wmsg));
			MessageBoxT(nullptr, wmsg.c_str(), e.path.wstring().c_str(), MB_OK | MB_ICONERROR);
			rc = E_EWRITE;
		}
		catch (const std::exception& e) {
			const std::string& msg{ e.what() };
			std::wstring wmsg;
			std::copy(msg.cbegin(), msg.cend(), std::back_inserter(wmsg));
			MessageBoxT(nullptr, wmsg.c_str(), SrcPath, MB_OK | MB_ICONERROR);
			rc = E_EWRITE;
		}
	}

	return rc;
}

void __stdcall ConfigurePacker(HWND ParentWin, HINSTANCE hinst) {
#if defined (CONGIG_MUTEX)
	std::lock_guard<std::shared_timed_mutex> wLock(configMutex);
#endif
	Config config{ iniFileName.c_str() };
	if (const auto rc{ ::DialogBoxParamW(hinst, MAKEINTRESOURCE(IDD_CONFIG), ParentWin, DlgProc, reinterpret_cast<LPARAM>(&config)) }; IDOK == rc) {
		config.Write();
	}
}

void __stdcall PackSetDefaultParams(PackDefaultParamStruct* dps) {
	PackDefaultParamStructW dpsW;
	ZeroMemory(&dpsW, sizeof(PackDefaultParamStructW));
	dpsW.PluginInterfaceVersionHi = dps->PluginInterfaceVersionHi;
	dpsW.PluginInterfaceVersionLow = dps->PluginInterfaceVersionLow;
	dpsW.size = sizeof(PackDefaultParamStructW);
	awlcopy(dpsW.DefaultIniName, dps->DefaultIniName, wdirtypemax);
	PackSetDefaultParamsW(&dpsW);
}

void __stdcall PackSetDefaultParamsW(PackDefaultParamStructW* dps) {
	::InitCommonControls();
	if (dps) {
		iniFileName = dps->DefaultIniName;
	}
}
