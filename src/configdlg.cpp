#include "stdafx.h"
#include "configdlg.h"
#include "config.h"
#include "resource.h"

INT_PTR CALLBACK DlgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
	case WM_INITDIALOG:
	{
		SetWindowLongPtrW(hWnd, GWLP_USERDATA, static_cast<LONG_PTR>(lParam));
		auto config = reinterpret_cast<Config*>(lParam);
		SendDlgItemMessageW(hWnd, IDC_MIN, BM_SETCHECK, config->setMin ? BST_CHECKED : BST_UNCHECKED, 0);
		SendDlgItemMessageW(hWnd, IDC_ALL, BM_SETCHECK, config->ignoreAllDirs ? BST_CHECKED : BST_UNCHECKED, 0);
		SendDlgItemMessageW(hWnd, IDC_EMPTY, BM_SETCHECK, config->ignoreEmptyDirs ? BST_CHECKED : BST_UNCHECKED, 0);
		EnableWindow(GetDlgItem(hWnd, IDC_EMPTY), !config->ignoreAllDirs);
		break;
	}

	case WM_CLOSE:
		EndDialog(hWnd, IDCANCEL);
		break;

	case WM_COMMAND:
	{
		auto config = reinterpret_cast<Config*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));

		switch (LOWORD(wParam)) {
		case IDC_ALL:
			EnableWindow(GetDlgItem(hWnd, IDC_EMPTY), !SendDlgItemMessageW(hWnd, IDC_ALL, BM_GETCHECK, 0, 0));
			break;

		default:
			break;
		}

		if (wParam == IDOK) {
			if (config) {
				config->setMin = SendDlgItemMessageW(hWnd, IDC_MIN, BM_GETCHECK, 0, 0);
				config->ignoreAllDirs = SendDlgItemMessageW(hWnd, IDC_ALL, BM_GETCHECK, 0, 0);
				config->ignoreEmptyDirs = SendDlgItemMessageW(hWnd, IDC_EMPTY, BM_GETCHECK, 0, 0);
			}
			EndDialog(hWnd, IDOK);
		}
		else if (wParam == IDCANCEL) {
			EndDialog(hWnd, IDCANCEL);
		}
		break;
	}

	default:
		return FALSE;
	}

	return TRUE;
}
